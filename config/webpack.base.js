const ExtractTextPlugin = require('extract-text-webpack-plugin'),
      HtmlPlugin = require('html-webpack-plugin')

module.exports = {
  entry: './src/index.js', // archivo de entrada
  resolve: {
    extensions: ['.js', '.jsx', '.json', '.scss', 'gif', 'jpg', 'png'],
    alias: {
      scss: __dirname + '/../src/assets/scss',
      img: __dirname + '/../src/assets/img',
    }
  },
  module: {
    rules: [
      {
        "test": /\.jsx?$/, // archivos terminados en .js
        "exclude": __dirname + "/node_modules", // excluir...
        "loader": "babel-loader" // Usar el loader (babel-loader)
      },
      {
        test: /\.scss$/, // archivos terminados en .scss
        use: ['css-hot-loader'].concat( // HMR para estilos
          ExtractTextPlugin.extract({ // extraer css del bundle
            use: ['css-loader', 'sass-loader', 'postcss-loader'], // loaders a usar
            fallback: 'style-loader', // alternativa para cualquier CSS no extraído
          })
        )
      },
      {
        test: /\.(gif|png|jpe?g|svg)$/i,
        use: [
          'file-loader',
          {
            loader: 'image-webpack-loader',
            options: {
              bypassOnDebug: true,
            },
          },
        ],
      }
    ]
  },
  plugins: [
    new ExtractTextPlugin("estilos.css"), // nombre del archivo al cual extraer los estilos
    new HtmlPlugin({ // Plugin para generar html
      title: 'App title',
      filename: 'index.html',
      template: 'src/assets/index.ejs'
    }),
  ]
}
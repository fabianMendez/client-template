const baseConfig = require('./webpack.base')

module.exports = {
  ...baseConfig,
  output: { // salida
    path: __dirname + '/../public', // ruta
    filename: 'bundle.js' // nombre del archivo
  },
  devServer: {
    contentBase: __dirname + '/public', // directorio desde el cual servir el contenido
    historyApiFallback: true, // alternativa a ./index.html para SinglePageApplications
    inline: true, // modo en-linea
    open: false // abrir navegador por defecto al iniciar
  },
  devtool: 'eval-source-map'
}
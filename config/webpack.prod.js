const UglifyJsPlugin = require('uglifyjs-webpack-plugin'),
OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin')

const baseConfig = require('./webpack.base')

module.exports = {
...baseConfig,
output: { // salida
path: __dirname + '/../dist', // ruta
filename: 'bundle.min.js' // nombre del archivo
},
plugins: baseConfig.plugins.concat([
new UglifyJsPlugin(), // Minificar js
new OptimizeCssAssetsPlugin(), // Minificar css
])
}
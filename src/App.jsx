import React from 'react'

import city from 'img/city.jpg'

const App = () => (
  <div>
  	<h1>Hello world!</h1>
  	<img src={city}/>
  </div>
)

export default App